﻿using System.Windows;
using System.Windows.Input;

namespace DragAndBounce
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void MainCanvas_OnTouchDown(object sender, TouchEventArgs e)
        {
            var element = e.Source as FrameworkElement;
            if (!Equals(element, MainCanvas))
                return;

            TestControl.MoveAnim(e.GetTouchPoint(MainCanvas).Position);
        }

        private void MainCanvas_OnMouseDown(object sender, MouseButtonEventArgs e)
        {
            var element = e.Source as FrameworkElement;
            if (!Equals(element, MainCanvas))
                return;

            TestControl.MoveAnim(e.GetPosition(null));
        }
    }
}
