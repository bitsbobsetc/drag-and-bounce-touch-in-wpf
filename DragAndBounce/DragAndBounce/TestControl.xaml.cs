﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media.Animation;

namespace DragAndBounce
{
    public enum HorizontalEdgeBounceType
    {
        None,
        Left,
        Right
    }

    public enum VerticalEdgeBounceType
    {
        None,
        Top,
        Bottom
    }

    /// <summary>
    /// Interaction logic for TestControl.xaml
    /// </summary>
    public partial class TestControl : UserControl
    {
        private const int MoveMenuAnimationDurationMs = 200;

        private Storyboard _storyBoard;
        private DoubleAnimationUsingKeyFrames animationLeft;
        private DoubleAnimationUsingKeyFrames animationTop;

        public TestControl()
        {
            InitializeComponent();

            Loaded += OnLoaded;
        }

        private HorizontalEdgeBounceType CurrentHorizontalBounce { get; set; }

        private VerticalEdgeBounceType CurrentVerticalBounce { get; set; }

        private void OnLoaded(object sender, RoutedEventArgs routedEventArgs)
        {
            ManipulationStarting += ParentCanvasOnManipulationStarting;
            ManipulationDelta += ParentCanvasOnManipulationDelta;

            ManipulationInertiaStarting += ParentCanvasOnManipulationInertiaStarting;
            ManipulationCompleted += ParentCanvasOnManipulationCompleted;
        }

        private void ParentCanvasOnManipulationInertiaStarting(object sender, ManipulationInertiaStartingEventArgs args)
        {
            var element = args.OriginalSource as FrameworkElement;
            if (element == null)
            {
                return;
            }

            //args.TranslationBehavior.DesiredDeceleration = 40.0 * 96.0 / (1000.0 * 1000.0);
            args.TranslationBehavior.DesiredDeceleration = 20.0 * 96.0 / (1000.0 * 1000.0);
            args.Handled = true;
        }

        private void ParentCanvasOnManipulationCompleted(object sender, ManipulationCompletedEventArgs manipulationCompletedEventArgs)
        {
            CurrentHorizontalBounce = HorizontalEdgeBounceType.None;
            CurrentVerticalBounce = VerticalEdgeBounceType.None;
        }

        private void ParentCanvasOnManipulationDelta(object sender, ManipulationDeltaEventArgs args)
        {
            var element = args.OriginalSource as FrameworkElement;

            if (element != null)
            {
                if (args.IsInertial)
                {
                    HandleHorizontalBounce(args);
                    HandleVerticalBounce(args);

                    args.Handled = true;
                    return;
                }

                var currentLeft = Canvas.GetLeft(this);
                Canvas.SetLeft(this, currentLeft + args.DeltaManipulation.Translation.X);

                var currentTop = Canvas.GetTop(this);
                Canvas.SetTop(this, currentTop + args.DeltaManipulation.Translation.Y);

                args.Handled = true;
            }
        }

        private void ParentCanvasOnManipulationStarting(object sender, ManipulationStartingEventArgs args)
        {
            args.ManipulationContainer = ParentCanvas;
            args.Handled = true;
        }

        public static readonly DependencyProperty ParentCanvasProperty = DependencyProperty.Register(
            "ParentCanvas", typeof (Canvas), typeof (TestControl), new PropertyMetadata(default(Canvas)));

        public Canvas ParentCanvas
        {
            get { return (Canvas) GetValue(ParentCanvasProperty); }
            set { SetValue(ParentCanvasProperty, value); }
        }

        public void MoveAnim(Point newPoint)
        {
            //Animate to this new position
            _storyBoard = new Storyboard
            {
                AutoReverse = false,
                Duration = new Duration(TimeSpan.FromMilliseconds(MoveMenuAnimationDurationMs))
            };

            var currentLeft = Canvas.GetLeft(this);
            var newLeft = newPoint.X - (this.ActualWidth / 2);

            var currentTop = Canvas.GetTop(this);
            var newTop = newPoint.Y - (this.ActualHeight / 2);

            var easingFrameLeftStart = new EasingDoubleKeyFrame(currentLeft, new TimeSpan(0, 0, 0))
            {
                EasingFunction = new CircleEase() { EasingMode = EasingMode.EaseOut }
            };
            var easingFrameLeftEnd = new EasingDoubleKeyFrame(newLeft,
                new TimeSpan(0, 0, 0, 0, MoveMenuAnimationDurationMs))
            {
                EasingFunction = new CircleEase() { EasingMode = EasingMode.EaseOut }
            };
            
            var easingFrameTopStart = new EasingDoubleKeyFrame(currentTop, new TimeSpan(0, 0, 0))
            {
                EasingFunction = new CircleEase() { EasingMode = EasingMode.EaseOut }
            };
            var easingFrameTopEnd = new EasingDoubleKeyFrame(newTop,
                new TimeSpan(0, 0, 0, 0, MoveMenuAnimationDurationMs))
            {
                EasingFunction = new CircleEase() { EasingMode = EasingMode.EaseOut }
            };

            animationLeft = new DoubleAnimationUsingKeyFrames();
            animationLeft.KeyFrames.Add(easingFrameLeftStart);
            animationLeft.KeyFrames.Add(easingFrameLeftEnd);

            Storyboard.SetTarget(animationLeft, this);
            Storyboard.SetTargetProperty(animationLeft, new PropertyPath("(Canvas.Left)"));

            _storyBoard.Children.Add(animationLeft);

            animationTop = new DoubleAnimationUsingKeyFrames();
            animationTop.KeyFrames.Add(easingFrameTopStart);
            animationTop.KeyFrames.Add(easingFrameTopEnd);

            Storyboard.SetTarget(animationTop, this);
            Storyboard.SetTargetProperty(animationTop, new PropertyPath("(Canvas.Top)"));

            _storyBoard.Children.Add(animationTop);

            _storyBoard.Completed += storyBoard_Completed;
            _storyBoard.FillBehavior = FillBehavior.Stop;

            _storyBoard.Begin();
        }

        void storyBoard_Completed(object sender, EventArgs e)
        {
            Canvas.SetLeft(this, Canvas.GetLeft(this));
            Canvas.SetTop(this, Canvas.GetTop(this));
        }

        private void HandleVerticalBounce(ManipulationDeltaEventArgs args)
        {
            var currentTop = Canvas.GetTop(this);

            //Update the current Vertical bounce if necessary
            if (currentTop < 0)
            {
                CurrentVerticalBounce = VerticalEdgeBounceType.Top;
            }
            else if (currentTop + ActualHeight >= ParentCanvas.ActualHeight)
            {
                CurrentVerticalBounce = VerticalEdgeBounceType.Bottom;
            }

            if (CurrentVerticalBounce != VerticalEdgeBounceType.None)
            {
                //Apply the new Translation taking into account we have bounced vertically
                switch (CurrentVerticalBounce)
                {
                    case VerticalEdgeBounceType.Top:
                        Canvas.SetTop(this, currentTop + Math.Abs(args.DeltaManipulation.Translation.Y));
                        break;
                    case VerticalEdgeBounceType.Bottom:
                        Canvas.SetTop(this, currentTop - Math.Abs(args.DeltaManipulation.Translation.Y));
                        break;
                }
            }
            else
            {
                //Just apply the Translation as normal
                Canvas.SetTop(this, currentTop + args.DeltaManipulation.Translation.Y);
            }
        }

        private void HandleHorizontalBounce(ManipulationDeltaEventArgs args)
        {
            var currentLeft = Canvas.GetLeft(this);

            //Update the current Horizontal bounce if necessary
            if (currentLeft < 0)
            {
                CurrentHorizontalBounce = HorizontalEdgeBounceType.Left;
            }
            else if (currentLeft + ActualWidth >= ParentCanvas.ActualWidth)
            {
                CurrentHorizontalBounce = HorizontalEdgeBounceType.Right;
            }

            if (CurrentHorizontalBounce != HorizontalEdgeBounceType.None)
            {
                //Apply the new Translation taking into account we have bounced horizontally
                switch (CurrentHorizontalBounce)
                {
                    case HorizontalEdgeBounceType.Left:
                        Canvas.SetLeft(this, currentLeft + Math.Abs(args.DeltaManipulation.Translation.X));
                        break;
                    case HorizontalEdgeBounceType.Right:
                        Canvas.SetLeft(this, currentLeft - Math.Abs(args.DeltaManipulation.Translation.X));
                        break;
                }
            }
            else
            {
                //Just apply the Translation as normal
                Canvas.SetLeft(this, currentLeft + args.DeltaManipulation.Translation.X);
            }
        }
    }
}